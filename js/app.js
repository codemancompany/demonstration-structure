'use strict'

var app = angular.module( 'app', [ 'ngRoute', 'yaokiski' ] )

.config( [ '$routeProvider', '$locationProvider', function( $routeProvider, $locationProvider ) {
	$routeProvider
	.when( '/', {
		"templateUrl":	'structure/login.html'
	} )
	.when( '/dashboard', {
		"templateUrl":	'structure/dashboard.html'
	} )
	.otherwise( {
		"redirectTo":	'/'
	} );

	// $locationProvider.html5Mode( true );
} ] )

.controller( 'MainController', function( $controller, $scope ) {
	 $controller( 'YaokiskiController', { "$scope": $scope } );
	 $scope.test = 'kuetspali';
} );